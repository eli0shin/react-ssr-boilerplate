import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { create } from 'axios';

import reducers from '../client/reducers';

const API_URL = 'http://react-ssr-api.herokuapp.com';

export default req => {
  const axiosInstance = create({
    baseURL: API_URL,
    headers: { cookie: req.get('cookie') || '' },
  });
  return createStore(
    reducers,
    {},
    applyMiddleware(thunk.withExtraArgument(axiosInstance)),
  );
};
