import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';

const styles = {
  right: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    cursor: 'pointer',
    userSelect: 'none',
  },
  menuButtons: {
    color: 'white',
  },
};

const RightButtons = () => (
  <div style={styles.menuButtons}>
    <NavLink to="/users">
      <FlatButton>Users</FlatButton>
    </NavLink>
    <NavLink to="/admins">
      <FlatButton>Admins</FlatButton>
    </NavLink>
  </div>
);

const Header = (props, context) => (
  <AppBar
    title="React SSR"
    showMenuIconButton={false}
    titleStyle={styles.title}
    onTitleClick={() => context.router.history.push('/')}
  >
    <div style={styles.right}>{RightButtons(props)}</div>
  </AppBar>
);

Header.contextTypes = {
  router: PropTypes.object,
};

const mapStateToProps = state => ({ auth: state.auth });

export default connect(mapStateToProps)(Header);
