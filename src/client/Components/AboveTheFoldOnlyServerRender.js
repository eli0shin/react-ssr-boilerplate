import React from 'react';
import PropTypes from 'prop-types';

const AboveTheFoldOnlyServerRender = props => {
  if (process.env.PORT) {
    return props.placeholder || <div />;
  }
  return props.children;
};

AboveTheFoldOnlyServerRender.defaultProps = {
  skip: true,
};

AboveTheFoldOnlyServerRender.PropTypes = {
  placeholder: PropTypes.element,
  skip: PropTypes.bool,
};

export default AboveTheFoldOnlyServerRender;
