import React from 'react';
import { connect } from 'react-redux';
// import { Redirect } from 'react-router-dom';

export default WrappedComponent => {
  const RequireAuth = props => {
    switch (props.auth) {
      case false:
        return (
          <div>
            You are not authenticated, please log in to view
            this page
          </div>
        );
      // return <Redirect to="/" />;
      case null:
        return <div>Loading...</div>;
      default:
        return <WrappedComponent {...props} />;
    }
  };

  const mapStateToProps = state => ({
    auth: state.auth,
  });

  return connect(mapStateToProps)(RequireAuth);
};
