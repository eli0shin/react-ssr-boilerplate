import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { fetchUsers } from '../actions';

const styles = {
  container: {
    minHeight: '100vh',
    maxWidth: 1200,
    marginRight: 'auto',
    marginLeft: 'auto',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 30,
  },
  h2: {
    fontSize: 20,
    marginBottom: 10,
  },
};

class UsersList extends Component {
  componentWillMount() {
    this.props.fetchUsers();
  }

  renderUsers = () =>
    this.props.users.map(user => (
      <li key={user.id}>{user.name}</li>
    ));

  head() {
    return (
      <Helmet>
        <title>
          {`React SSR | ${this.props.users.length} Users`}
        </title>
        <meta
          property="og:title"
          content="React SSR | Users"
        />
        <meta
          property="og:description"
          content={this.props.users.map(
            user => `${user.name}`,
          )}
        />
      </Helmet>
    );
  }

  render() {
    return (
      <div style={styles.container}>
        {this.head()}
        <h2
          style={styles.h2}
        >{`Here's a big list of users:`}</h2>
        {this.renderUsers()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users,
});

export default {
  component: connect(mapStateToProps, { fetchUsers })(
    UsersList,
  ),
  loadData: ({ dispatch }) => dispatch(fetchUsers()),
};
