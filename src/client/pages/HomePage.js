import React from 'react';
import AboveTheFoldOnlyServerRender from '../Components/AboveTheFoldOnlyServerRender';

const styles = {
  container: {
    paddingTop: 200,
    textAlign: 'center',
  },
  heading: {
    fontSize: 32,
  },
};

const Home = () => (
  <div style={styles.container}>
    <div style={styles.heading}>{`Welcome Home`}</div>
    <AboveTheFoldOnlyServerRender>
      <div>
        <h3>
          This text will only render on the client side
        </h3>
      </div>
    </AboveTheFoldOnlyServerRender>
  </div>
);

export default { component: Home };
