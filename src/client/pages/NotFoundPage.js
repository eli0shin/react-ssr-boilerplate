import React from 'react';

const styles = {
  container: {
    paddingTop: 200,
    textAlign: 'center',
  },
  h1: {
    display: 'block',
    fontSize: 28,
  },
  h2: {
    display: 'block',
    fontSize: 20,
  },
};

const NotFoundPage = ({ staticContext = {} }) => {
  staticContext.notFound = true; // eslint-disable-line no-param-reassign

  return (
    <div style={styles.container}>
      <h1 style={styles.h1}>404</h1>
      <h2 style={styles.h2}>Page Not Found</h2>
    </div>
  );
};

export default {
  component: NotFoundPage,
};
