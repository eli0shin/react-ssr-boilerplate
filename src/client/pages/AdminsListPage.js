import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAdmins } from '../actions';
import requireAuth from '../Components/hocs/requireAuth';

class AdminsListPage extends Component {
  componentDidMount() {
    this.props.fetchAdmins();
  }

  renderAdmins = () =>
    this.props.admins.map(({ id, name }) => (
      <li key={id}>{name}</li>
    ));

  render() {
    return (
      <div>
        <h3>Protected List of Admins</h3>
        <ul>{this.renderAdmins()}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({ admins: state.admins });

export default {
  component: connect(mapStateToProps, { fetchAdmins })(
    requireAuth(AdminsListPage),
  ),
  loadData: ({ dispatch }) => dispatch(fetchAdmins()),
};
