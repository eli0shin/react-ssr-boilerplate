import React from 'react';
import { renderRoutes } from 'react-router-config';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { fetchCurrentUser } from './actions';

import Header from './Components/Header';

const styles = {
  container: {
    minHeight: '100vh',
    fontFamily: 'roboto',
  },
};

const userAgent = headers =>
  headers
    ? headers['user-agent']
    : window.navigator.userAgent; // eslint-disable-line no-undef

const muiTheme = headers =>
  getMuiTheme({
    userAgent: userAgent(headers),
  });

const App = props => (
  <MuiThemeProvider muiTheme={muiTheme(props.headers)}>
    <div style={styles.container}>
      <Header />
      <div>{renderRoutes(props.route.routes)}</div>
    </div>
  </MuiThemeProvider>
);

export default {
  component: App,
  loadData: ({ dispatch }) => dispatch(fetchCurrentUser()),
};
