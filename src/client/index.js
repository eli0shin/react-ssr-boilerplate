// startup point for the client side
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { create } from 'axios';

import reducers from './reducers';
import Routes from './Routes';

const axiosInstance = create({
  baseURL: '/api',
});

const store = createStore(
  reducers,
  window.INITIAL_STATE, // eslint-disable-line no-undef
  applyMiddleware(thunk.withExtraArgument(axiosInstance)),
);

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <div>{renderRoutes(Routes)}</div>
    </BrowserRouter>
  </Provider>,
  document.querySelector('#react-root'), // eslint-disable-line no-undef
);
