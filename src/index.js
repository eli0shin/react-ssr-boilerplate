import 'babel-polyfill';
import dotenv from 'dotenv';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import proxy from 'express-http-proxy';
import compression from 'compression';
import Routes from './client/Routes';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';

dotenv.config();

const API_URL = process.env.API_URL;
const PORT = process.env.PORT || 8080;

const app = express();

app.use(compression());

app.use(
  '/api',
  proxy(API_URL, {
    proxyReqOptDecorator(opts) {
      return {
        ...opts,
        headers: {
          ...opts.headers,
          'x-forwarded-host': `glacial-scrubland-90453.herokuapp.com/:${PORT}`,
        },
      };
    },
  }),
);

app.use(express.static('public'));

app.get('*', (req, res) => {
  const store = createStore(req);
  const promises = matchRoutes(Routes, req.path).map(
    ({ route }) =>
      route.loadData ? route.loadData(store) : null,
  );

  const wrappedPromises = promises.map(promise => {
    if (promise) {
      return new Promise(resolve => {
        promise.then(resolve).catch(resolve);
      });
    }
    return null;
  });

  return Promise.all(wrappedPromises)
    .then(() => {
      const context = {};
      const response = renderer(req, store, context);
      // if we set a redirect url in context ( possibly because the user tried to access a protected route)
      if (context.url) {
        return res.redirect(301, context.url);
      }
      // if the user goes to a route that doesn't exist we set a notFound property on context
      if (context.notFound) {
        res.status(404);
      }
      return res.send(response);
    })
    .catch(err => {
      console.log(err);
      res.send('An error has occurred');
    });
});

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
