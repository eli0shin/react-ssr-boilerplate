const path = require('path');
const merge = require('webpack-merge');
const webpackNodeExternals = require('webpack-node-externals');

const baseConfig = require('./webpack.base.js');

const config = {
  // Inform webpack that we are building a bundle for node.js
  target: 'node',
  // Tell webpack the root file of our server application
  entry: './src',
  // Tell webpack where to save the output file after building our bundle
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
  },
  externals: [webpackNodeExternals()],
};

module.exports = merge(baseConfig, config);
