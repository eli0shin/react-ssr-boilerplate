const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');

const config = {
  // Tell webpack the root file of our client  application
  entry: {
    client: './src/client',
    vendor: [
      'react',
      'react-dom',
      'react-router-dom',
      'redux',
      'react-redux',
      'axios',
      'material-ui',
      'react-router-config',
      'redux-thunk',
    ],
  },
  devtool: 'inline-source-maps',
  // Tell webpack where to save the output file after building our bundle
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'public'),
  },
};

module.exports = merge(baseConfig, config);
