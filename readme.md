# React Server Side Rendering Boilerplate

This boilerplate focuses on writing code that will run on both the browser and server without the need for repetition.

Using `babel` and `webpack` we are able to write code with full ES6 stage-0 support.

Code formatting in this project is highly opinionated. Using a combination of `Prettier` and `es-lint` syntax and style errors will be higlighted in real-time. Code will be auto-formatted on-save in vs-code. This feature can easily be added to any code editor.

---

## Getting Started

```bash
mkdir <project name>
cd <project name>
git clone <url of repo> ./
yarn
touch .env
```

Inside the `.env` file that you just created define your environment variables as follows:

```bash
API_URL=<base path of your API>
PORT=3000
```

* You can use the `PORT` of your choosing as long as it is not already in use.

* Environment variables are read in your application from `process.env.<NAME_OF_VARIABLE>`

Once complete the dev server can be started with:

```bash
yarn dev
```

The React app will be available at `http://localhost:3000`

---

## Structure

### Directory Structure

```tree
src/
|--index.js
|--helpers/
| |--createStore.js
| `--renderer.js
|
`--client/
   |--actions/
   |--Components/
   |  |--hocs/
   |  `--Layout/
   |
   |--pages/
   |--reducers/
   |--App.js
   |--index.js
   `--Routes.js
```

* The server entry point is `src/index.js` while the the client entry point is at `src/client/index.js`.

* All code in the client directory can be run on either the server or browser, barring the above `index.js` file.

* The `src/helpers/` directory holds helper functions required for server-side rendering.
  * `createStore.js` for initiating the redux store
  * `renderer.js` for rendering the page

### Page and Route Structure

Considering the fact the React SSR aims to emulate a closer experience to a static web page, we want to split out individual routes into pages.

* These pages are stored in the `src/client/pages/` directory.

* Layout 'partials' are stored in `src/client/Components/Layout/`

* Basic universal page structure (header, footer, etc.) can be acheived in the `App.js` component

Routing is handled via `react-router-config` in `src/client/Routes.js`

* In order to allow for server-side data fetching the `default` `export` from pages must be an object containing a `component` property.
  * You can also include a `function` under a `loadData` property to call action creators on the server.
  * Being that we are loading data on the server side outside of the component lifecycle, action creators to load initial data should be called in `componentDidMount()`

Component example

```js
export default {
  component: HomePage,
};
```

Container example

```js
export default {
  component: connect(mapStateToProps, { fetchUsers })(
    HomePage,
  ),
  loadData: ({ dispatch }) => dispatch(fetchUsers()),
};
```

When including the page in the `Routes.js` file use the object-stread operator to include all the properties that you exported

```js
{ path: '/', ...HomePage, exact: true },
```

Pages may use the `requireAuth` HOC to require that a user be authenticated to navigate to a route. This will protect both the server and browser sides of the application.

```diff
  component: connect(mapStateToProps, { fetchUsers })(
-    HomePage,
+    requireAuth(HomePage),
  )
```

---

## Actions Creators and other XHR Requests

In order to provide a truely universal experience, a custom instance of `axios` is created for both the server and client side.

```js
const axiosInstance = axios.create({
  baseURL: '/api',
});
```

This instance is then passed to `redux-thunk` using `withExtraArgument` in `createStore()`

```js
applyMiddleware(thunk.withExtraArgument(axiosInstance)),
```

It can be used in action creators with `redux-thunk` as follows:

```js
export const fetchUsers = () => async (
  dispatch,
  getState,
  api,
) => {
  const res = await api.get('/users');

  dispatch({
    type: FETCH_USERS,
    payload: res,
  });
};
```

### API Authentication

* All authentication for the API server is proxied through the rendering server using `express-http-proxy`
  * This requires that all API calls are made through the rendering server
  * The API base-url should be included in your `.env` file as `API_URL`
  * On the server your `API_URL` for requests will be as defined in `.env`
  * In the browser the `API_URL` will be that of the rendering server
* This is accomplished as follows:
  * Custom headers can be added to the proxied transmission as illustrated in `proxyReqOptDecorator()`

```js
app.use(
  '/api',
  proxy(API_URL, {
    proxyReqOptDecorator(opts) {
      return {
        ...opts,
        headers: {
          ...opts.headers,
          'x-forwarded-host': `localhost:${PORT}`,
        },
      };
    },
  }),
);
```

### Programmatic Navigation with React Router

Router methods can be accessed via `context`. This requires defining `contextTypes` which necessitates importing the `prop-types` library.

```js
import PropTypes from 'prop-types';

const Header = (props, context) => {
  <Button
    onClick={() => context.router.history.push('/')}
  />;
};

Header.contextTypes = {
  router: PropTypes.object,
};
```

---

## Performance

### Code-Splitting

This is a must.

* Look into webpack route based code-splitting and loading with `<link rel="preload />`
* We are currently only splitting universal vendor packages

### ATF Rendering

Can be done using `AboveTheFoldServerRendering` Component located in the `Components/` directory.

Children of this component will only be rendered on the client side. This is very usefull in reducing the number of components that we are asking the server to render. Technically we only need to render on the server components that appvear above the fold.

* The `skip` `prop` can be dynamically set to `false` to allow rendering on server side

Usage:

```jsx
<div>
  <h3>
    This text will render on the server.
  </h3>
</div>
<AboveTheFoldOnlyServerRender>
  <div>
    <h3>This text will only render on the client side</h3>
  </div>
</AboveTheFoldOnlyServerRender>
```

### Caching

Very dangerous but worth exploring.

* Look into how Walmart Labs is doing it with Electrode
  * <https://github.com/electrode-io/electrode-react-ssr-caching>

### Streaming

Probably not a good idea because it introduces major issues with responding with `404`/`401` status code, redirects, etc.

### NODE_ENV

### Other Enhancements

* `babel-plugin-transform-react-constant-elements`
* `babel-plugin-transform-react-inline-elements`

---

## Building For Production

* setup webpack for creating production builds

---

## Git

This Project could probably benefit from using two git repositories

* one for development
* one for production

This would allow us to only push built files to production

---

## Webpack HTML Template Generation

This will allow us to use hash naming for our bundles that is dynamically injected using webpack. It will allow for long term caching
